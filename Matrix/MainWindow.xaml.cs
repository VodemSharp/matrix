﻿using Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Matrix
{
    /// <summary>
    /// Represents the main window of the application
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            //string[][] array = new string[10][];

            //for (int i = 0; i < 10; i++)
            //{
            //    array[i] = new string[10];
            //    for (int j = 0; j < 10; j++)
            //    {
            //        array[i][j] = "";
            //    }
            //}

            //dataGrid.ItemsSource = array;

            //for (int i = 0; i < 5; i++)
            //{
            //    dataGrid.Columns.Add(new System.Windows.Controls.TextBox());
            //    dataGrid.Items.Add(new System.Windows.Controls.TextBox());
            //}

            //List<System.Windows.Controls.TextBox> tableData = new List<System.Windows.Controls.TextBox>(5);
            //dataGrid.ItemsSource = tableData;
        }
    }
}
